---
comments:       false
showDate:       false
showSocial:     false
showTags:       false
showPagination: false
draft: false
title: "Spolupracujeme"
---


{{< image classes="fancybox center" src="/img/spolupracujeme/cakovice-ikonka.jpg" thumbnail-width="80px" thumbnail="" group="group:travel" title="Městská část Praha - Čakovice">}}

{{< image classes="fancybox center" src="/img/spolupracujeme/adc-systems.png"  thumbnail-width="80px" thumbnail="" group="group:travel" title="ADC Systems">}}

{{< image classes="fancybox center" src="/img/spolupracujeme/autodoprava.png"  thumbnail-width="80px" thumbnail="" group="group:travel" title="Luboš Enc - Autodoprava">}}

{{< image classes="fancybox center" src="/img/spolupracujeme/zscakovice_logo.png"  thumbnail-width="80px" thumbnail="" group="group:travel" title="ZŠ Dr. Edvarda Beneše a družina při této škole">}}

{{< image classes="fancybox center" src="/img/spolupracujeme/ddm-logo.jpg"  thumbnail-width="80px" thumbnail="" group="group:travel" title="DDM Praha 9 - Čakovice">}}





