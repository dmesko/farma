---
comments:       false
showDate:       false
showSocial:     false
showTags:       false
showPagination: false
draft: false
title: "Centrum ekologické výchovy Farma Miškovice, z.s."
---

Jsme spolek chovatelů a milovníků přírody, kteří by rádi přiblížili dětem i dospělým svět domácích i hospodářských zvířat, jejich vliv a vazby na člověka i přírodu celkově a budovali v nich pozitivní vztah k životu, přírodě a ekologii.

Naším cílem je tedy propagace ekologické výchovy, rozvoj dětí a mládeže, podpora chovatelství, péče o floru a faunu, jejich ochranu a poskytování obecně prospěšné činnosti v oblasti ekologické výchovy veřejnosti.