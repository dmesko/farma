---
comments:       false
showDate:       false
showSocial:     false
showTags:       false
showPagination: false
draft: false
title: "Pro veřejnost"
---

Rodiče i děti k nám mohou přijít prakticky kdykoli. Ideální je ale předem zavolat, jsme-li v dosahu.
Děti si u nás mohou pod dozorem chovatelů, nebo rodičů projít farmičku, osahat si všechny druhy chovaných zvířat, posbírat vajíčka, pomazlit se v králičí ohradě a vyvenčit a nakrmit kozičky.
Volně přístupné je i nově zrekonstruované hřiště s herní věží, skluzavkami, houpačkou a malým brouzdalištěm.

**Pozor!** Přes den hlídá zahradu velký pes! Hlídá ostře, ale nekouše (tedy zatím). Je zde proto, aby nikdo nechodil ke zvířatům bez dozoru a nekrmil je něčím, co by jim mohlo ublížit.
Zkuste zazvonit. Pokud jsme doma, bude pes odvolán a zavřen a vy srdečně přivítáni.
Nejsme-li doma, zatelefonujte nám!

{{< image classes="fancybox left fig-50" src="/img/verejnost/verejnost1.jpg" thumbnail="" group="group:travel" >}}
{{< image classes="fancybox left fig-50" src="/img/verejnost/verejnost2.jpg" thumbnail="" group="group:travel" >}}