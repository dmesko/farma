---
comments:       false
showDate:       false
showSocial:     false
showTags:       false
showPagination: false
draft: false
title: "Plány"
---

Během roku 2017 bychom rádi vybudovali malou kontaktní farmičku na pozemku obce (viz plánek) vhodnou pro dětské návštěvníky s možností přístupu dětí k vybraným zvířatům.

Součástí komplexu bude i klubovna s prostorem na výstavy a s chovatelským zázemím a hlavně toaletami, do budoucna i mateřský klub a herna.
Plánujeme chovatelské kroužky, odborné přednášky, tematické výstavy.

{{< image classes="fancybox fig-100" src="/img/plany/plany.jpg" group="group:plany">}}
