---
comments:       false
showDate:       false
showSocial:     false
showTags:       false
showPagination: false
draft: false
title: "Pro školy"
---

Pořádáme komentované prohlídky farmičky s výkladem a lehkou svačinkou.
Děti se od zkušených chovatelů dozvědí nejen jaké druhy chováme, a jak se o ně starat, ale také zajímavosti z jejich života, případně původu druhu, naučí se jak se zvířaty manipulovat a jak se k nim chovat. Zároveň se seznámí s pracemi okolo zvířat a náčiním k tomu potřebným. Výklad doprovázen otázkami, zapojením dětí, a hlavně přímým kontaktem s krmením, nářadím, zvířaty...

Vhodné pro skupiny do max. 25 dětí – děti rozdělíme na dvě skupiny, přičemž jedna si hraje s doprovodem na hřišti a krmí kozičky, druhá absolvuje prohlídku a výklad. V menší skupině jsou děti pozornější a zvířata klidnější, snadněji se dostane při předvádění na každého.

Vše přizpůsobeno věku dětí – školkáčci, předškoláci, mladší školáci a starší školáci.

{{< image classes="fancybox left fig-33" src="/img/skoly/skoly1.jpg" thumbnail="" group="group:travel" >}}
{{< image classes="fancybox left fig-33" src="/img/skoly/skoly2.jpg" thumbnail="" group="group:travel" >}}
{{< image classes="fancybox left fig-33" src="/img/skoly/skoly3.jpg" thumbnail="" group="group:travel" >}}