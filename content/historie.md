---
comments:       false
showDate:       false
showSocial:     false
showTags:       false
showPagination: false


title: "Historie"
draft: false
---

Naše farma byla založena před pěti roky (**2012**) převzetím opuštěného dvorku s několika králíky, slepicemi a kočkami. Zvířátka jsme zaopatřili, přidali pár našich a rozhlásili, že existujeme. Za rok se u nás sešlo několik dalších, většinou nechtěných morčat, králíků i slepiček, přibyly kachny a koza Jůlinka. 

Druhým rokem (**2013**) k nám kromě sousedů a dětí začaly po domluvě docházet děti z mateřských školek, škol a rodiny z širšího okolí. Přibyla i darovaná koza Nelinka. Odchovali jsme první kachňátka, kuřátka i méně chtěná koťátka, divokou matku jsme nechali kastrovat. A přes zimu jsme vypiplali malinké ježky.

Třetím rokem (**2014**) přibyl kozel Mazel (darovaný, zachráněný před pekáčem) a začalo velké množení. Snad poslední koťata (kastrace další polodivoké kočky), kuřata (25), kachňata (téměř 40), králíčata (15) a nakonec 31. srpna kozlíček Flíček.

Čtvrtý rok (**2015**) jsme naši činnost rozšířili, zvýšila se návštěvnost škol, školek a zájmových kroužků a proto jsme založili Centrum ekologické výchovy Farma Miškovice, z.s. (zapsaný spolek). Díky skvělé spolupráci s obcí rozjíždíme přípravy na vybudování ekologického centra s malou kontaktní zoo hospodářských zvířat a klubovnou se sociálním zázemím, chovatelským zázemím, kuchyňkou a hernou. Dostáváme do zápůjčky od obce pozemek a nemovitost vhodnou pro tyto účely.

Pátým rokem (**2016**) jsme přestavěli stávající dvorek, zrekonstruovali a zjednodušili. Začíná realizace opravy zapůjčené nemovitosti a přípravy pozemku pro budoucí zoo (viz plány a záměry).
