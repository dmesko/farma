---
comments:       false
showDate:       false
showSocial:     false
showTags:       false
showPagination: false
title: "Kontakt"
draft: false
---

Telefonní spojení - 603 981 180, 731 086 749

Email - info@farmamiskovice.cz

Facebook – [Centrum ekologické výchovy Farma Miškovice](https://www.facebook.com/farmamisko/)

Centrum ekologické výchovy Farma Miškovice, z.s.  
IČ: 04256034  
Spolek je zapsán ve spolkovém rejstříku vedeném u Městského soudu v Praze,  
značka L 63182.

Sponzorské dary na chod Farmy a krmení zvířat můžete zasílat na č. ú.: 42560348/5500