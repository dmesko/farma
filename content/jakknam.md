---
comments:       false
showDate:       false
showSocial:     false
showTags:       false
showPagination: false
title: "Jak k nám"
draft: false
---

CEV Farma Miškovice se nachází v obci Miškovice, Praha 9

Jak se k nám dostanete? 
Spoj MHD linka 140, 202 nebo 377, zastávka Miškovice.

Autem od Čakovic ulicí Polabskou, od Kbel a Vinoře ulicí Všetatskou do obce Miškovice. Na křižovatce u zastávky pak boční ulicí ke sběrně. Pro navigaci můžete použít adresu Polabská 140/43, Miškovice.

{{< googleMap >}}


