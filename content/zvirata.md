---
comments:       false
showDate:       false
showSocial:     false
showTags:       false
showPagination: false
draft: false
title: "Naše zvířata"
---

### Kozy

Jůlinka – koza (*2012) – kříženec kozy domácí, původně určený na pekáč, koupená jako osmiměsíční v červnu 2012. 


Mazlík – (*2012) darovaný dvouletý kamerunský kozlík ze svíčkárny Rodas v Šestajovicích (kde by musel být časem vyeliminován). U nás rok chovný, po té vykastrovaný.

{{< image classes="fancybox fig-50 left" src="/img/zvirata/_DSC0577_s.jpg" thumbnail="" group="group:travel">}}
{{< image classes="fancybox fig-50 left" src="/img/zvirata/_DSC0565_s.jpg" thumbnail="" group="group:travel">}}
{{< image classes="fancybox fig-100 left clear" src="/img/zvirata/_DSC0526_s.jpg" thumbnail="" group="group:travel">}}

### Králíci

Pepan – (*2012) černobílý chovný samec – český strakáč z Jižních Čech.

Skvrnka – samice druhu český strakáč

Kaliforňanka – samice druhu Kalifornský velký králík

Zrzek – samec český červený králík

Beranka – samice zakrslého černého beránka

A mazlící omladina…

{{< image classes="fancybox fig-50 left" src="/img/zvirata/DSCF2086_s.jpg" thumbnail="" group="group:travel">}}
{{< image classes="fancybox fig-50 left" src="/img/zvirata/DSCF2082_s.jpg" thumbnail="" group="group:travel">}}
{{< image classes="fancybox fig-50 left" src="/img/zvirata/_DSC0531_s.jpg" thumbnail="" group="group:travel">}}
{{< image classes="fancybox fig-50 left clear" src="/img/zvirata/DSCF2073_s.jpg" thumbnail="" group="group:travel">}}

### Morčata

Zrzinka – červená rozeta

Dupík – hladkosrstý samec trikolor

Trixi – rozeta trikolor, dcera 

Čip a Dejl - synové Zrzky a Dupíka

{{< image classes="fancybox left fig-50" src="/img/zvirata/DSCF2096_s.jpg" thumbnail="" group="group:travel" >}}
{{< image classes="fancybox left fig-50 clear" src="/img/zvirata/DSCF2102_s.jpg" thumbnail="" group="group:travel">}}

### Slepice

Pepinka – ochočená, kontaktní, hodná, kuře z DDM z roku 2015

Pepík – velký kohout, ochočený, ale k dětem a psům agresivní (voliéra)

Hejno australek – Australky jsou velmi klidné masné slepice se schopností snášet i v zimě. 

Barevné slípky – kříženci, kuřata z roku 2015

Zakrslé slepičky – brahmanky
{{< image classes="fancybox left fig-50" src="/img/zvirata/_DSC0512_s.jpg" thumbnail="" group="group:travel" >}}
{{< image classes="fancybox left fig-50" src="/img/zvirata/DSCF2089_s.jpg" thumbnail="" group="group:travel" >}}
{{< image classes="fancybox left fig-50" src="/img/zvirata/kropenata.jpg" thumbnail="" group="group:travel" >}}
{{< image classes="fancybox left fig-33 clear" src="/img/zvirata/_DSC0538_s.jpg" thumbnail="" group="group:travel" >}}


### Kachny

Kachna pižmová – varianta bílá a černobílá.

{{< image classes="fancybox fig-50" src="/img/zvirata/_DSC0547_s.jpg" thumbnail="" group="group:travel">}}
{{< image classes="fancybox fig-50" src="/img/zvirata/_DSC0557_s.jpg" thumbnail="" group="group:travel">}}
{{< image classes="fancybox fig-25" src="/img/zvirata/_DSC0555_s.jpg" thumbnail="" group="group:travel">}}
{{< image classes="fancybox fig-75 clear" src="/img/zvirata/_DSC0582_s.jpg" thumbnail="" group="group:travel" >}}